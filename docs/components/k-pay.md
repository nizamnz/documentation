# k-pay

!!! attention "Note"
    k-pay is in public Beta. If you face any issue reach us at: [support@getkitsune.com](mailto:support@getkitsune.com)

Enables you to to integrate payment gateway on any element. Includes two attributes that can be used on any HTML element:
```k-pay-amount``` and ```k-pay-description```.

!!! question "How does it work?"
    When the element with ```k-pay-amount``` is clicked the user is redirected to payment gateway with necessary parameters. After completion of payment, the user lands back at ```redirect_path```.

### Usage
- Add ```k-pay-amount``` and ```k-pay-description``` attributes to any element.  
```html
<a k-pay-amount="20" k-pay-description="T-Shirt">Proceed to Pay</a>
```
You can specify currencies explicitly, otherwise it would be assumed as INR.
Note: The currency specified should be supported by the gateway.
```html
<a k-pay-amount="USD:7" k-pay-description="Product in USD">Checkout</a>
<a k-pay-amount="INR:474.84" k-pay-description="Product in INR">Checkout</a>
<a k-pay-amount="INR:474.84;USD:7;" k-pay-description="Multi Currency">Checkout</a>
```
- Call ```kpay.open()```  
You need to include the sdk ```https://payments.kitsune.tools/library/k-pay.js``` in your dynamic page.  
```html
<script src=//payments.kitsune.tools/library/k-pay.js charset=utf-8 defer=defer async=async>
<script>
    (function() {
        kpay.open({
            charges: [
                { currency: "INR", amount: 200 },
                { currency: "USD", amount: 15 }
            ],
            description: "Donation to Water on Wheels Campaign",
            gateway: "PADDLE"
        });
    ))();
</script>
```

### Configuration
Add the following payment gateway specific configuration to _kitsune-settings.json_ file in the project root directory.


!!! info
    Currently only [Instamojo](#instamojo), [PayU biz](#payu-biz) and [Paddle](#paddle) integrations are available.  
    More payment gateways are being added regularly.

#### Instamojo
Get the ```SALT```, ```API KEY``` and ```API TOKEN``` from [Instamojo > API & Plugins](https://www.instamojo.com/integrations)

Currencies supported: INR  

Note: you need to signup and generate test ```api_key```, ```api_token``` separately with Instamojo test account [https://test.instamojo.com](https://test.instamojo.com)

```json
{
    "payments": {
        "preview": [
            {
                "domain": "example.com",
                "gateway": "instamojo",
                "salt": "SALT",
                "api_key": "API_KEY",
                "api_secret": "API_SECRET",
                "redirect_path": "/transaction_status",
                "api_url": "https://test.instamojo.com/api/1.1"
            }
        ],
        "live": [
            {
                "domain": "example.com",
                "gateway": "instamojo",
                "salt": "SALT",
                "api_key": "API_KEY",
                "api_secret": "API_SECRET",
                "redirect_path": "/transaction_status",
                "api_url": "https://www.instamojo.com/api/1.1"
            }
        ]
    }
}
```
#### PayU biz
Get the ```SALT```, ```API KEY``` and ```API TOKEN``` from [PayU biz](https://www.payubiz.in/)

Currencies supported: INR  

Note: For a preview, you can use global payu biz test account by  ```api_key: gtKFFx```, ```api_token: eCwWELxi``` separately with Payu test account [https://test.payu.in](https://test.payu.in)

```json
{
    "payments": {
        "preview": [
            {
                "domain": "example.com",
                "gateway": "payu",
                "api_key": "gtKFFx",
                "salt": "eCwWELxi",
                "api_secret": "",
                "redirect_path": "/transaction_status",
                "api_url": "https://test.payu.in"
            }
        ],
        "live": [
            {
                "domain": "example.com",
                "gateway": "payu",
                "api_key": "API_KEY",
                "salt": "SALT",
                "api_secret": "",
                "redirect_path": "/transaction_status",
                "api_url": "https://secure.payu.in"
            }
        ]
    }
}
```
#### Paddle
Get the ```VENDOR_ID``` and ```AUTH_CODE``` from [Paddle > Vendor Settings > Integrations](https://vendors.paddle.com/account)

Currencies supported: INR, USD, EUR and many [more](https://paddle.com/support/what-currencies-do-you-support)  
Note: Only three currencies can be specified at a time.  

Note: There are no separate test credentials for Paddle. To check the entire flow, you can set the amount to 0.

```json
{
    "payments": {
        "preview": [
            {
                "domain": "example.com",
                "gateway": "paddle",
                "api_key": "VENDOR_ID",
                "api_secret": "AUTH_CODE",
                "redirect_path": "/transaction_status",
                "api_url": "https://vendors.paddle.com/api/2.0"
            }
        ],
        "live": [
            {
                "domain": "example.com",
                "gateway": "paddle",
                "api_key": "VENDOR_ID",
                "api_secret": "AUTH_CODE",
                "redirect_path": "/transaction_status",
                "api_url": "https://vendors.paddle.com/api/2.0"
            }
        ]
    }
}
```
Paddle supports multiple currencies. You can mention all the currencies in ```k-pay-amount``` attribute by following the format ```CURRENCY_CODE1:AMOUNT2;CURRENCY_CODE2:AMOUNT2;```

e.g. 
```html
<a k-pay-amount="INR:300;USD:6;EUR:9.5;" k-pay-description="Coffee Beans">
```  

### Showing Transaction Status to User
The SDK provides three kinds of events to listen to:

1. kpaysuccess

2. kpayfail

3. kpayinprogress

using which you can inform the user about the status of the payment.

You need to include the sdk ```https://payments.kitsune.tools/library/k-pay-status.js``` in your dynamic page.

#### Sample Status Page
```html
<head k-dl="/transaction_status"></head>
<body>
    Transaction Complete.
    <div id="transaction_status">
        FETCHING STATUS ...
    </div>

    <script src="//payments.kitsune.tools/library/k-pay-status.js" type="text/javascript" charset="utf-8" defer></script>
    <script type="text/javascript" charset="utf-8">
        document.addEventListener('kpaysuccess', function (txn) {
            var transaction_status_el = document.getElementById('transaction_status');
            transaction_status_el.textContent = 'Transaction Status: SUCCESS';
            console.log(txn.detail);
        });

        document.addEventListener('kpayfail', function (txn) {
            var transaction_status_el = document.getElementById('transaction_status');
            transaction_status_el.textContent = 'Transaction Status: Fail';
            console.log(txn.detail);
        });

        document.addEventListener('kpayinprogress', function (txn) {
            var transaction_status_el = document.getElementById('transaction_status');
            transaction_status_el.textContent = 'Transaction Status: In Progress';

            var reload_el = document.createElement('a');
            reload_el.href = 'javascript:location.reload();';
            reload_el.textContent = 'refresh status';

            transaction_status_el.appendChild(document.createElement('br'));
            transaction_status_el.appendChild(reload_el);
            console.log(txn.detail);
        });
    </script>
</body>
```

### Advanced
#### Response Webhook
If you need a trigger when Payment Gateway responds with a Webhook which can be mentioned in the `kitsune-settings.json` file as:
```json hl_lines="8"
{
    ...
    "payments": {
        "preview": [
            {
                "domain": "*",
                ...
                "response_webhook": "http://api.example.com/your_api"
            }
        ]   
    }
}
```
The link specified in `response_webhook` gets a http `POST` request with the transaction data in the following format:
```json
{
    "payment_request_id": "REQ123456",
    "transaction_id": "TXN123456",
    "payment_url": "https://www.paymentgateway.com/pay/REQ123456",
    "description": "New Year Gifts",
    "amount": 100.00,
    "currency": "INR",
    "status": "SUCCESS",
    "is_complete": true,
    "remarks": "Webhook Received from Gateway",
    "buyer_email": "john@example.com",
    "buyer_phone": "9876543210",
    "buyer_name": "John Doe",
    "buyer_address": "123, Address Road, City, Country",
    "_payment_gateway": "INSTAMOJO",
    "_env": "preview",
    "_gateway_response": { ... },
    "requested_charges": [
        {
            "amount": 100.0,
            "currency": "INR"
        }
    ],
    "details": {
        "key": "value"
    },
}
```

#### Storing additional details in transaction
If you want to retain certain information with each transaction e.g. OrderId, you can pass them into `details` key like the following:
```html hl_lines="11 12 13"
<script src=//payments.kitsune.tools/library/k-pay.js charset=utf-8 defer=defer async=async></script>
<script>
    (function() {
        kpay.open({
            charges: [
                { currency: "INR", amount: 200 },
                { currency: "USD", amount: 15 }
            ],
            description: "Donation to Water on Wheels Campaign",
            gateway: "PADDLE",
            details: {
                "key": "value"
            }
        });
    ))();
</script>
```
`details` key will accept any javascript object. You will be able to access the `details` object inside the transaction object available on transaction status page:
```javascript hl_lines="6"
document.addEventListener('kpaysuccess', function (txn) {
    var transaction_status_el = document.getElementById('transaction_status');
    transaction_status_el.textContent = 'Transaction Status: SUCCESS';
    var transaction = txn.detail;

    console.log(transaction.details);
});
```