#Advanced Kitsune Tags

With kitsune, you give your client the complete power over the content on the website. But you can still control how that content will be displayed.

Kitsune provides six tags for this purpose.

Kitsune Tag | Description 
----------- | ------------- 
[**k-hide**](k-hide-tag.md) | Hides the content if the condition is **true**
[**k-show**](k-show-tag.md) | Shows the content only if the condition is **true**
[**k-repeat**](k-repeat-tag.md) | Displays all or few items
[**k-norepeat**](k-norepeat-tag.md) | Displays the content only once
[**k-dl**](k-dl-tag.md) | Create dynamic URL of a web page. 
[**k-object**](k-object-tag.md) | Creates an object for a class to be used on that page
[**k-script**](tags/k-script.md) | To display data from API response
