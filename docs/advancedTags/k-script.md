# k-script

To display data populated by an external API

!!! Note
    The API should return data in the form of an array of objects.
    For example:
    
    ```
    [
      {
        "name": "Segment",
        "domain": "segment.com",
        "logo": "https://logo.clearbit.com/segment.com"
      },
      {
        "name": "Segmentify",
        "domain": "segmentify.com",
        "logo": "https://logo.clearbit.com/segmentify.com"
      }
    ]
    ```
    
## 'GET' API without any headers

**Code example:** 

```
<k-script get-api="http://autocomplete.clearbit.com/v1/companies/suggest?query=Segment">
    <div k-repeat="[[kresult,i,0:kresult.length()]]">
        <span>
            [[kresult[i].name]]
        </span>
        <a href="[[kresult[i].domain]]">
            <img src="[[kresult[i].logo]]" alt="[[kresult[i].name]]">
        </a>
    </div>
</k-script>
```

## 'GET' API with headers

**Code example:**

```
<k-script get-api="http://autocomplete.clearbit.com/v1/companies/suggest?query=Segment"
        headers="[Authorization: username]">
    <div k-repeat="[[kresult,i,0:kresult.length()]]">
        <span>
            [[kresult[i].name]]
        </span>
        <a href="[[kresult[i].domain]]">
            <img src="[[kresult[i].logo]]" alt="[[kresult[i].name]]">
        </a>
    </div>
</k-script>
```
