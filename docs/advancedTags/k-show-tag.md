#k-show tag

Shows the content if the condition is **True**.
   
**Scenario 1:**

To add a default image in case no image is added with a blog post.

Using k-show tag you can simply check if the image url returned is null or not and give the default image.


```
    <div class="date" k-show="[[coffeeshop.blogpost[0].blogimage.url == null || coffeeshop.blogpost[0].blogimage.url == '']]">
        <img src="/images/dynamic/coffee_cup.png">
    </div>
```
This will only show the specified image if there is no image in the update on k-admin/manage or if the url returned by kitsune backend is null.

![k-show with coffee theme to show default image](/images/tags/k-show-coffee-theme.JPG)

!!! info
    When kitsune compiles your HTML page, if the condition given with the k-show tag is true, only then shows that HTML tag in its children. Giving your website a seamless appearance :D

**Scenario 2:**

To show some default message if there are no blog posts currently added on the webpage.

***Code example: ***

```
<div k-hide="[[coffeeshop.blogpost.length() > 0]]">
    <p>Keep watching this space for information about our awesome products.</p>
</div>
```

This will show the message if there are no products available on k-admin.

!!! tip
    You can use k-hide and k-show together to give conditions like: hide the section if no items are available and show some default message/image in its place.

    When the client adds the items on k-admin, then the default message disappears and the item(s) will come up.