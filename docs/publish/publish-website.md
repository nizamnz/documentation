# Publish your Website

**Ready to go live?**

Just click on the **Publish button** option on your project menu to publish your website on Kitsune.

![publish1](/images/publish/choosePublish.png)

## Minimum Funds
!!! warning "Wallet Balance required!"
    To publish it is necessary to add a minimum amount of Rs. 300 to the wallet.
    To add money, just click on the [add more funds](/publish/add-funds.md) button on the top right of the dashboard.

## Publish Choices 
If you have the minimum required funds in your account then you will be asked about the type of publishing you would like to go ahead with.

![1](/images/publish/publishChoiceType.png)

![3](/images/publish/writeSubdomain.png)
        
Select **any of the options** and proceed with the selection. For now, we will select as a kitsune sub-domain.

!!!note
    If you want to use your **subdomain** or custom domain, you can select the second domain and then map your root DNS **A** record to the Kitsune IP and we will internally direct traffic. Or you can add a **CNAME** Record mapping your **root(@)** record to the subdomain **{yourChoice}.getkitsune.com** .

## Customer Details
![3.1](/images/publish/enterPublishDetails.png)

        
After filling up the customer details, click on the proceed button. If you are sure that you want to publish.


## Final Confirmation and Live

Click on **yes** button in the next dialog box that appears.

![3](/images/publish/confirmPublish.png)

!!! note
    Publish might take some time to process depending on the size of your website.


After processing is done, go to the **live sites** tab on the dashboard. 

You will see all your published sites listed in that tab, from there just click on the website menu and then **open site** and you can view your **live website** hosted on Kitsune!

![published_projects_list](/images/gettingStarted/dnsOpen.png)

## Info

By default, any website hosted on Kitsune is accessible by getkitsune domain, which has the form **[name of website].getkitsune.com**. For example, you would access a website named ‘bluemasters’ with bluemasters.getkitsune.com.

!!! success
    Now your website is live. Go grab some rest and have fun. You have earned it. See you in the next tutorial.