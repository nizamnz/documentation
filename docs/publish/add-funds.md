# Add Funds
You need funds to publish your website on Kitsune. And to make this process hassle free we have an easy add fund option in Kitsune which makes it easy to add funds and publish sites. 

To add money to your Kitsune wallet:



1) Click on the **ADD MONEY** button to add money to your wallet and fill in the billing information in the form that pops up.

![add_more_funds](/images/publish/addmoney1.png)
        
2) After filling up your billing information, click on the "Proceed" button, you will be asked to enter the amount you would like to add to your wallet.

![add_billing_information](/images/publish/addmoney2.png)

        
3) Click on the "add money" button, you will be redirected to the payment gateway where you can pay using Credit/Debit Card, Net Banking or UPI.

![add_money](/images/publish/addmoney3.png)

Complete the payment and voila! Now you can publish your website.

![payment_gateway](/images/publish/addmoney4.png)