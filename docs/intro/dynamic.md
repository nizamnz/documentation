# Dynamic Websites

Websites today must be cost-effective, optimized for speed & size globally, get discovered organically on search engines, enable secure & fast payment collection process, enable changing content dynamically and much more. For building such a website, you will have to learn at least 6 different web technologies.
Kitsune enables you to build the most powerful websites using simple HTML. No backend coding required! Websites deployed on Kitsune are serverless, and hence the most cost-effective - you only pay if your website is being used by your customers.

With Kitsune creating a dynamic website is easy and doesn't need you to master backend programming nor handling the database. Kitsune helps you work on creating your dynamic website only with the knowledge of **HTML** code. 

## Creating a data model
![schema](/images/intro/schema.png)

Creating a data model is a breeze. You can select the components and their data types using a visual builder to create a data model. This data model can be used throughout your website for accessing the data components. 

## Using data on the website
![htmlcode](/images/intro/html_based_coding.png)

With Kitsune you can use HTML components to build dynamic websites. This makes making dynamic websites as easy as using bootstrap. You can create dynamic data injection, payment initiation, and a ton of other dynamic functionalities. 

Not only that, forget using Backend Languages to create loops and buisness conditions in your website. With Kitsune you can use loops and conditions directly in HTML.

**Example**

```
    <ul k-repeat="[[item in coffeeshop.products]]">
        <li><a href="#"><span class="item_name">[[item.name]]</span><span class="price">$[[item.price]]</span></a></li>
    </ul>
```
**is equals to** 

```
    <div class="col_1_of_4 span_1_of_4">
    <h3>Morning</h3>
    <img src="images/special-img1.jpg" alt="" />
    <ul>
        <li><a href="#"><span class="item_name">Nam elit agna,endrerit</span><span class="price">$32</span></a></li>
        <li><a href="#"><span class="item_name">Tincidunt ac, viverra</span><span class="price">$41</span></a></li>
        <li><a href="#"><span class="item_name">Donec porta diam </span><span class="price">$28</span></a></li>
        <li><a href="#"><span class="item_name">Tincidunt ac, viverra</span><span class="price">$41</span></a></li>
        <li><a href="#"><span class="item_name">Quisque diam</span><span class="price">$56</span></a></li>
        <li><a href="#"><span class="item_name">Interdum vitae,dap    </span><span class="price">$64</span></a></li>
        <li><a href="#"><span class="item_name">Lacinia fermentum</span><span class="price">$34</span></a></li>
        <li><a href="#"><span class="item_name">Suspendisse velit</span><span class="price">$34</span></a></li>
        <li><a href="#"><span class="item_name">Tincidunt ac</span><span class="price">$41</span></a></li>
        <li><a href="#"><span class="item_name">Ut pharetra ugue</span><span class="price">$64</span></a></li>
        <li><a href="#"><span class="item_name">viverra sed</span><span class="price">$45</span></a></li>
        <li><a href="#"><span class="item_name">Donec in velit </span><span class="price">$35</span></a></li>
        <li><a href="#"><span class="item_name">Ut pharetra augue</span><span class="price">$60</span></a></li>
    </ul>
    </div>
```
And this is one of the many tags that you can use to make your business website more and more productive.
For more head on to the **tags** Section of the Docs from [here](/advancedTags/advanced-kitsune-tags/).
## Highly intuitive CMS
![cms](/images/intro/adaptive_cms.png)

Content Management System (CMS) is a very critical part of a dynamic website, as it makes it easy for the website owner to create/edit content on the go. The Kitsune CMS or K-Admin allows you to manipulate data interactions in your website. Kitsune automatically creates a CMS for every website, based on the dynamic data models defined by the developer. This gives complete control of the look & feel of the CMS to the developer.

