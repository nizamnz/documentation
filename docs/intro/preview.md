# Preview your Website

Kitsune offers an easy way to check your website using the preview button. Preview is available on the project card in the Kitsune dashboard. Clicking on preview opens the website on a new tab to help you check upon your project.

![preview](/images/intro/preview.png)

This will spin up a new tab to show your current website. 
![screensize](/images/intro/screensize.png)

!!! note
    You can use the top frame to check the compatibility of your website across various screen sizes. Kitsune offers Web, Tablet and Mobile size testing at the click of a button.

# Publishing your Website

Publishing your website for the world to see is done in Kitsune using a click. Kitsune needs funds for publishing your website, which you can add funds in the top right corner. Adding your funds is facilitated by instamojo. Once you have sufficient funds your website goes live, powered by Kitsune. 

![preview](/images/intro/fund.png)

# Backup and Download 
 
 Kitsune ensures that your website is backed up and secure, but in case of a human error or for the peace of mind, you can download a copy of your website. Kitsune automatically collects all your website resources and zips them up neatly and initiates the download. You can initiate the download by clicking on the options button in your project card. 
 
 ![screensize](/images/intro/download.png)

 The options menu also offers the **upload** option. Though we love our web IDE and we believe its one of the best out there, for all the offline IDE lovers, the upload button enables you to edit your downloaded project and upload it back to make the necessary changes.

!!! note
    Be careful of the **delete** button, it deletes your project and all associates resources linked with it.