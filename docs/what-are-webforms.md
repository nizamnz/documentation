# What are webforms?

The dynamic forms available on a website are what we call Webforms. For example **login form** or a **contact form**. With a static website you cannot store or keep track of the form data in case a customer wants to contact you or wants to send an enquiry.

Using kitsune Webforms, you can just add the JS script for your form (which Kitsune will provide) and be done with it, no need of maintaining a database just for this purpose **:)**


![modal_contact_us_form](images/modal_contact_us_form.png)