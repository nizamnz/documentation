#Advanced kitsune functions

Here are some of the functions that kitsune supports:

Advanced Kitsune Functions | Description 
--------------------- | ------------- 
[**urlencode()**](language/function/urlencode)       | This function will help in encoding url to proper format which browser understands.
[**indexOf('word')**](language/function/indexof)		  | This function returns the position of the first occurrence of a specified value in a string.
[**replace('/','-')**](language/function/replace/)     | This function will replace the specified value with the new value in the entire string.
[**length()**](language/function/length/)          | Length would give length of the object. The object can be string, array or list.
[**subStr(0, 10)**](language/function/substr/) | This function would be operated only on string. This will return substring of a string from index 0 to 10. 
[**subStr(10)**](language/function/substr/)      | This function would be operated only on string. This will return substring of a string from index 10 to length of string.
[**toLower()**](language/function/tolower/)         | This function works on string. This function will convert entire string to lowercase.
[**toUpper()**](language/function/toupper/)         | This function works on string. This function will convert entire string to uppercase.

