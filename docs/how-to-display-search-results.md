#Creating the search page

First create the url pattern for the search page using k-dl tag.

For example: ```<head k-dl="search/[[Search.modifidedsearchtext.urlencode()]]//[[View('pageName.html.dl').currentpagenumber]]"></head>```

Search is the base class, which uses the result object to access the properties of the search results.

**Code example 1:**

```
//To display all search results of the itemtype product
<div k-repeat="[[SearchResult in Search.result]]">
  <div k-show="[[SearchResult.itemtype.tolower()=='product']]">
    <h2> [[SearchResult.title]] </h2>
	<p>
      <a href="[[View('product-single.html.dl').setObject(SearchResult.data)]]">
	 	[[SearchResult.description.SubStr(0,250)]] 
	  </a>
	</p>
  </div>
</div>
```

This will display all the results of the item type products.

**Code example 2:**

```
//To display all search results of the itemtype update
<div k-repeat="[[SearchResult in Search.result]]">
  <div k-show="[[SearchResult.itemtype.tolower()=='update']]">
   	<h2> [[SearchResult.title]] </h2>
	 <p>
	  <a href="[[View('blog-single.html.dl').setObject(SearchResult.data)]]">
	   [[SearchResult.description.SubStr(0,250)]]
	  </a>
	 </p>
  </div>
</div>
```

This will display all the results of the item type update.

You can use both of these code examples to display results of update and product itemtype on the same page

!!! note "You can also"
	You can use paginations on search page the same way you used them on other pages!
	With the help of offset, previouspage and nextpage properties.