
**What is Kitsune class in kitsune?**

Kitsune Class is the logical structure of the data that you can use to wrap the related data fields.

Class contain following meta info 

* **Name** : name of the class
    - unique name within the language
    - it can contain only alphanumeric and '_'(underscore)
    - it can not start with _ or number
    - The maximum length of the class name is limited to 50 characters
* **Description** : Description of the class(will be visible to end customer in the K-Admin)
    - Can be any string
    - Limit is 100 characters
* **GroupName** : Class group name
    - Group multiple class into one to make it more manageable in k-Admin
* **ReferenceType** : Relation between two object
    - You can define class reference by two way
        1. reference by **Value**
            - If the class is reference by value type, the object which is created from the source object will not be updated on updation of the source object. Its just replica of the source object and not linked. (Similar to shallow copy). 
        2. reference by **Reference**
            - If the class is reference by Reference type, the object which is created from the source object will be updated on the update of the source object. Its linked to the source object. Just like same memory location on the object-oriented concept. (Copy by reference).
* **Properties** : List of data fields for the class.
    - A class can have any number of properties with a specific type.
    - All the properties can be accessed in by its class reference in the Kitsune tags between `[[baseclass.propertyname.nested_property]]`
    - A property can be referenced to any other class by its object reference.
