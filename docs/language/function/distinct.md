###Functionality

Returns the unique values of system array elements.

###Syntax

`[[coffeeshop.testimonials.distinct()]]`

###How does it work?

The `distinct()` function returns the array(system datatype) of unique elements from the current array.

if the current array value does not exist or null, it will return default null.

###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | -----
str | name | sample coffeeshop
array(str) | testimonials | ['very good coffee', 'awesome ambiance', 'very good coffee']

source 
```html
<div>
    <!-- this will retun the array with unique testimonials value  -->
    <ul k-repeat="[[testimonial in coffeeshop.testimonials.distinct()]]">
        <li>[[testimonial]]</li>
    </ul>
</div>
```


output 
```html
<div>
    <ul>
        <li>very good coffee</li>
        <li>awesome ambiance</li>
    </ul>
</div>
```