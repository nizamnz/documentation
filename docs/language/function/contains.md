###Functionality

Returns true/false if the string contains the specified substring provided in function parameter.

###Syntax

`[[coffeeshop.name.contains('hello')]]`

###How does it work?

The contains() function returns the boolean(true/false) if the specified substring provided in function parameter is present in the string datatype.

if the value dose not exist or null, it will return default false.

this function is only supported with str datatype.

###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|----|----
str | name | sample coffeeshop
str | address | b/11, mg road

source
```html
<div>
    <!-- for str datatyp it will show 'This is coffeeshop' 
        only if the coffeeshop description contains 'coffee' -->
    <span k-show="[[coffeeshop.name.contains('coffee')]]">
        This is coffee shop
    </span>
      <span k-hide="[[coffeeshop.name.contains('coffee')]]">
        This is not coffee shop
    </span>
</div>
```
output
```html
<div>
    <span>
        This is coffee shop
    </span>
</div>
```