###Functionality

Returns a copy of current string by HTML decoding. 

###Syntax

`[[coffeeshop.address.decode()]]`

###How does it work?

The decode() function returns the decoded string of current string in the str datatype.

if the value does not exist or null, it will return an empty string.

this function is only supported with the str datatype.

###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- |-----
str | name | sample coffeeshop
str | address | &amp;#x3C;ul k-repeat=&amp;#x22;[[item in coffeeshop.products]]&amp;#x22;&amp;#x3E;

source
```html
<div>
    <!-- for str datatyp it will show '<span>b/11, sample coffee shop</span>' -->
    [[coffeeshop.address.decode()]]
</div>
```
output

```html
<div>
    <span>b/11, sample coffee shop</span>
</div>
```