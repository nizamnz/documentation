###Functionality

Returns a copy of this string converted to lowercase.

###Syntax

`[[coffeeshop.description.tolower()]]`

###How does it work?

The `tolower()` function will return the lowercase equivalent of the current string

if the value does not exist or null, it will return default null.


###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | ----
str | name | sample coffeeshop
str | description | Best place to get your mood Fresh & Connect

source
```html
<div>
    <span>[[coffeeshop.description.tolower()]]</span>
</div>
```

output 
```html
<div>
    <span>best place to get your mood fresh & connect</span>
s</div>
```