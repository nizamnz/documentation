###Functionality

 Retrieves a substring from current string. The substring starts at a specified character position and has a specified length

###Syntax

`[[coffeeshop.description.substr(7, 5)]]`
`[[coffeeshop.description.substr(7)]]` 

###How does it work?

The `substr(start_ind, (optional)length)` function will return the substring of the current string by zero-based starting character position 'start_ind' characters from the start and returns subsequent characters, if the length is present then it will returns only subsequent characters of specified length.

if the start_ind is greater than the length of the current string, then it will return an empty string.
if the length is exceeding the length of the string then it will return only till the length of the subsequent string.

if the value does not exist or null, it will return default null.


###Example
 
coffeeshop(base class) language properties

Type | Name | Value
-----|---- | ----
str | name | sample coffeeshop
str | description | best place to get your mood fresh & connect

source
```html
<div>
    <span>[[coffeeshop.description.substr(13)]]</span>
    <span>[[coffeeshop.description.substr(13, 8)]]</span>
</div>
```

output 
```html
<div>
    <span>get your mood fresh and connect</span>
    <span>get your</span>
</div>
```