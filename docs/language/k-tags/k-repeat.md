#k-repeat tag

Loops over or repeats the content of the HTML tag.

When you have 'n' number of products stored in the database, how will you design and style the page properly?

k-repeat can be used as a for loop or a for-each loop.

##k-repeat as for loop

**When can be used?**

To display specific number of sections on a menu page.

On the price.html in the coffee shop theme, you will see the price list of products divided by sections (Menu).

```html
<div class="col_1_of_4 span_1_of_4">
	<h3>Morning</h3>
	<img src="images/special-img1.jpg" alt="" />
	<ul>
		<li><a href="#"><span class="item_name">Nam elit agna,endrerit</span><span class="price">$32</span></a></li>
		<li><a href="#"><span class="item_name">Tincidunt ac, viverra</span><span class="price">$41</span></a></li>
		<li><a href="#"><span class="item_name">Donec porta diam </span><span class="price">$28</span></a></li>
		<li><a href="#"><span class="item_name">Tincidunt ac, viverra</span><span class="price">$41</span></a></li>
		<li><a href="#"><span class="item_name">Quisque diam</span><span class="price">$56</span></a></li>
		<li><a href="#"><span class="item_name">Interdum vitae,dap	</span><span class="price">$64</span></a></li>
		<li><a href="#"><span class="item_name">Lacinia fermentum</span><span class="price">$34</span></a></li>
		<li><a href="#"><span class="item_name">Suspendisse velit</span><span class="price">$34</span></a></li>
		<li><a href="#"><span class="item_name">Tincidunt ac</span><span class="price">$41</span></a></li>
		<li><a href="#"><span class="item_name">Ut pharetra ugue</span><span class="price">$64</span></a></li>
		<li><a href="#"><span class="item_name">viverra sed</span><span class="price">$45</span></a></li>
		<li><a href="#"><span class="item_name">Donec in velit </span><span class="price">$35</span></a></li>
		<li><a href="#"><span class="item_name">Ut pharetra augue</span><span class="price">$60</span></a></li>
	</ul>
</div>
```

If you notice the html code, this is the div tag which is used for all the sections. You can just give a k-repeat instead of adding the code for each section again and again.

**For example:**

```html
<div class="section group" k-repeat="[[coffeeshop.menu,i,0:4]]">
	<div class="col_1_of_4 span_1_of_4">
		<h3>[[coffeeshop.menu[i].header]]</h3>
		<img src="images/special-img1.jpg" alt="" />
		<ul>
			<li><a href="#"><span class="item_name">Nam elit agna,endrerit</span><span class="price">$32</span></a></li>
			<li><a href="#"><span class="item_name">Tincidunt ac, viverra</span><span class="price">$41</span></a></li>
			<li><a href="#"><span class="item_name">Donec porta diam </span><span class="price">$28</span></a></li>
			<li><a href="#"><span class="item_name">Tincidunt ac, viverra</span><span class="price">$41</span></a></li>
			<li><a href="#"><span class="item_name">Quisque diam</span><span class="price">$56</span></a></li>
			<li><a href="#"><span class="item_name">Interdum vitae,dap	</span><span class="price">$64</span></a></li>
			<li><a href="#"><span class="item_name">Lacinia fermentum</span><span class="price">$34</span></a></li>
			<li><a href="#"><span class="item_name">Suspendisse velit</span><span class="price">$34</span></a></li>
			<li><a href="#"><span class="item_name">Tincidunt ac</span><span class="price">$41</span></a></li>
			<li><a href="#"><span class="item_name">Ut pharetra ugue</span><span class="price">$64</span></a></li>
			<li><a href="#"><span class="item_name">viverra sed</span><span class="price">$45</span></a></li>
			<li><a href="#"><span class="item_name">Donec in velit </span><span class="price">$35</span></a></li>
			<li><a href="#"><span class="item_name">Ut pharetra augue</span><span class="price">$60</span></a></li>
		</ul>
	</div>
</div>
```


As a designer, you don't know how many sections or products the client might want to add in the future.
The client might have only 3 products and if you provide space for 5, then other 3 cards will look empty. With k-Repeat, you can tell the Kitsune upto how many times that HTML tag needs to be repeated. 

Kitsune will repeat all the children of the parent tag that has the k-Repeat tag given number of times or as many times as there is data (whichever comes first). 

##k-repeat as for-each loop

**When can be used?**

To display details of all the products.

```html  
<ul k-repeat="[[item in coffeeshop.products]]">
	<li><a href="#"><span class="item_name">[[item.name]]</span><span class="price">$[[item.price]]</span></a></li>
</ul>
```
![k-repeat as foreach loop in coffee theme](/images/k-repeat-foreach-loop-coffee-theme.JPG)

This will iterate or loop over all the products stored on k-admin and will display there name and price.