#k-object tag

To display details or information of a single item.

**k-object tag** creates an object for a class, which can be used throughout the page.

**Code example:**

```
	<html k-object="[[post in coffeeshop.blogpost]]">
		<head k-dl="[[post.title.substr(0,25).urlencode()]]/u[[post._id]]">
			<title> [[coffeeshop.name]] </title>
		</head>
		<body>...</body>
	</html>
```
This creates ```post``` object for updates class, which can be used throught the page for getting the information about a particular update.

```
<h3>[[post.title.substr(0,40)]]</h3>
<div class="image group">
	<div class="grid span_2_of_1">
		<p>[[post.title]]</p>
	</div>
</div>
```

![k-object for blog post on coffee theme](/images/blog-single-coffee-theme-k-object.jpg)