#k-partial tag

It helps you mark the begining of the section which is to be rendered as a partials section. 
k-partial tag in the parent div marks it as the div which is to be rendered as the partial. 

##Example

**When can be used?**

It can be used for any section that is used in multiple pages and stays the same across all of them.

```html
<body k-partial="">
 <div class="page-loader-wrapper">
   <div class="page-loader"></div>
 </div>
 <div class="hide k-info">
   <a id="home-url" href="[[Business.rootaliasurl.url]]"></a>
```

If you notice the html code, the HTML code has a body with a 'k-partial' tag which denotes the section to be a partial. Now when we call this page using partial() we will get the section defined in the k-partial. Thus this can be used to reduce code redundancy. 
