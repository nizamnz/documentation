#k-norepeat tag

To display certain content only once, when given in k-repeat, then k-norepeat is the tag for you!

**Code example:**

```html
	<div k-repeat="[[item in coffeeshop.section]]">
        <div class="items active" k-norepeat>
              <div class="row">
                   <p class="col-xs-12"> All </p>     
              </div>
        </div>
        <div class="items">
        	<div class="row">
        		<p class="col-xs-12">[[item.header]]</p>
        	</div>
        </div>
    </div>
```

This will display "All" only once and will continue iterating over products name

Output:

```

	All	 	Morning    Afternoon   Evening

```	 

