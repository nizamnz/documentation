### boolean property type

The `boolean` type is used to declare variables to store the Boolean values, true and false.

Kitsune expression will return **True/False** based on the place its used.

You can use boolean in the [`k-show`](/k-show-tag.md) and [`k-hide`](/k-hide-tag.md) tags.

##Example
coffeeshop(base class) language properties

Type | Name
-----|----
str | name
boolean | wifiAvailable

```html
<!-- just use in the expression -->
<div k-show="[[cofeeshop.wifiAvailable]]">Free Wifi available!</div>

<!-- Display the value -->
<div>[[coffeeshop.isopen]]</div>

```