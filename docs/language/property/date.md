### date property type

Date type can be used for storing datetime value. It can store the date and time in StandardDateTimeFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK")

you can perform date comparision operation in kitsune tags


##Example
coffeeshop (userdefined class) language properties

Type | Name
-----|----
str | name
array(str) | testimonials
array(offer) | offers
number |bookedTables 

offer (userdefined class)

Type | Name
-----|-----
title | str
decription | str
discountPercentage | number
startDate | date
endDate | date

```html
<span>
    <div k-repeat="[[coffeeshop.offers, ind,  View('/offers.html').offset : 10]]">
        <span>[[coffeeshop.offers[ind].title]]</span>
        <span k-show="[[coffeeshop.offers[ind].startDate > coffeeshop.offers[ind].endDate]]" />
            Offer start date should be less then end date.
        </span>
        <span>Discount : [[coffeeshop.offers[ind].discountPercentage]]%</span>
    </div>
</span>    
```
