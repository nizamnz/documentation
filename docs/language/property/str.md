### str class (system)

**str** class id system class which is by default available in kitsune language.

### functions

Function Name | Function Description
----------- | -------------
length() | Returns the length of the string. (number of characters)
indexOf(string/character) | Reports the zero-based index of the first occurrence of the specified character/string in this current.
substr(startind, endind) | Retrieves a substring from current string. The substring starts at a specified character position and continues to the end of the string. 
substr(startind) | Retrieves a substring from current string. The substring starts at a specified character position and has a specified length
toupper() | Returns a copy of current string converted to uppercase.
tolower() | Returns a copy of current string converted to lowercase.
urlencode() | Returns a copy of current string with url encoding. It will remove special characters and replace space with '-'(hyphen)
split(string) | Splits a string into array of substrings that are based on the characters/string provided in the input.
contains(string) | Returns true/false based on the input string is present in the current string. it will search case insensitive.
replace(string, string) | Returns a new string in which all occurrences of a specified string in the current instance are replaced with another specified string. 
decode() | Returns a copy of current string by html decoding. 



##Example
coffeeshop(base class) language properties

Type | Name
-----|----
str | name
str | address

```html
<span>
    <h1>[['Welcome to ' + cofeeshop.Name]]</h1>
    <div>Address  :  [[coffeeshop.address]]</div>
</span>    
```


###FAQs

- **How can I concatinate two string?**
    - You can concatinate two string by simply '+' operator in the kitsune expression.
    For example `[['Welcome to ' + cofeeshop.Name]]`