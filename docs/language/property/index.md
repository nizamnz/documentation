**What is kitsune class property?**

Kitsune property is a datafield that defines the type specific storage and access in kitsune tags and k-admin.
Kitsune property can be defined by following meta information.

* **Name** : name of property
    - unique name withing the class
    - it can contain only alphanumeric and '_'(underscore)
    - it can not start with _ or number
    - Maximum length of property name is limited to 50 characters
* **Description** : Description of the property(will be visible to end customer in the K-Admin)
    - Can be any string
    - Limit is 100 characters
* **GroupName** : Property group name
    - Group multiple property of same class in to one to make it more managable in k-Admin
* **PropertyType** : Datatype of the field
    - Following system datatyps are available in kitsune language
        - [str](/language/property/str.md) | string datatype
            - you can store any type of string data.
            - there is no charactor limit.
            - str datatype has in built functions (`substr`, `length`, `indexof`, `toupper`, `tolower`, `urlnecode`, `split`, `contains`, `replace`, `decode` )
        - [array](/language/property/array.md) | iterator type
            - array of any datatype (system or custom objects)
            - array can have any number of elements
            - array datatype has in built functions (`length`,`distinct`)
        - [number](/language/property/number.md) | numeric values
            - number can store any type of number (`integer`, `float`, `long`)
            - up to 64 bit memory
            - number datatype has in built functions (`tostring`)
        - [date](/language/property/date.md) | date value
            - date can store datetime value 
            - by default it will store datetime in UTC format
            - you can perform date comparison operation with standard format
        - [boolean](/language/property/boolean.md) | boolean value (true/false)
            - boolean can store true/false 
            - default value will be false
        - [kstring](/language/property/kstring.md) | string with automated keyword extraction feature
            - kstring is unique datatype of the kitsune language
            - using kstring you can store string and kitsune will dynamically extract meaningful keyworeds out of the string stored.
            - keywords extracted from the kstring can be used for better search engine optimization
            - kstring datatype has two property (`text` and  `keywords`). `text` is type of [str](/language/property/str.md) and `keywords` is type of [array](/language/property/array.md)
        - [object](/language/property/object.md) | data instance of the class
            - object datatype is used for createing instance of any class
            - you can store data in nested format. any property can be type of user defined class.
            - using object you can create custom property with class structure
            - object can be referenced by value or by referene based on the class definition 
 


