### array property type

array type can be used for storing collection of objects or value datatypes in the database.

using array you can store any number or elements in one collection. 

there are two types of array collection you can create

1. array of system datatype
2. array of custom datatype

### array of system datatype
- you can simply create array of system datatype like `str`, `number`, `date`, `boolean`

- using system datatype array you can iterate through all the elements by [k-repeat](/k-repeat-tag.md) (see examle). 

- in this type you can not use pagination for the elements

### array of custom datatype

- you can create custom dataype array by providing array type while language creation.

- using custom datatype array similar to the system array datatyp, you can iterate through [k-repeat](/k-repeat-tag.md).
- But more over you can also use this array for pagination using offset in k-repeat.(see in example) 

###Functions

array datatype provides following functions

Function Name |Function Description
 -------------|-----------------
 length() | Returns the length of array elements
 distinct() | Returns the distinct elements of the arry (only available for system datatype array) 


##Example
coffeeshop(base class) language properties

Type | Name
-----|----
str | name
array(str) | testimonials
array(offer) | offers
number |bookedTables 


offer (userdefined class)

Type | Name
-----|-----
title | str
decription | str
discountPercentage | number
startDate | date
endDate | date
```html
<span>
    <!--k-repeat withh system array datatype -->
    <div k-repeat="[[testmonial in coffeeshop.testmonials]]">
        <span>[[testmonial]]</span>
    </div> 
    <!-- k-repeat with custom array datatype, this will create offers list page with page size of 10 -->
    <div k-repeat="[[coffeeshop.offers, ind,  View('/offers.html').offset : 10]]">
        <span>[[coffeeshop.offers[ind].title]]</span>
        <span>Discount : [[coffeeshop.offers[ind].discountPercentage]]%</span>
    </div>
</span>    
```
