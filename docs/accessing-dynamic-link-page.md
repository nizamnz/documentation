#Accessing dynamic pages

To call a dynamic webpage kitsune provides two functions **setObject()** and **getUrl()**.

<hr>

##setObject() function

Lets us access the properties of the item type like the URL for the details page.

**Code example:**

```

<div k-repeat="[[coffeeshop.blogpost,i,0:10]]">
	<p>[[coffeeshop.blogpost[i].title.SubStr(0,100)]]</p>
	<a href="[[View('/blog-single.html.dl').setObject(coffeeshop.blogpost[i])]]">Read More..</a>
</div>

```

In the anchor tag's href attribute, setObject will set the URL of that particular update from the 'blog-single.html.dl' page's k-dl tag.

<hr>

##getUrl() function

Used to get the URL of a list page.

**Code example:**

```
<div k-repeat="[[coffeeshop.blogpost,i,0:10]]">
	<p>[[coffeeshop.blogpost[i].title.SubStr(0,100)]]</p>
	<a href="[[View('/blog-single.html.dl').setObject(coffeeshop.blogpost[i])]]">Read More..</a>
</div>
	 <span>
	 	<a href = "[[View('/blog.html.dl').getUrl()]]">Show All</a>
	 </span>
```

This will get the URL of the list page mentioned in the View() function, i.e. blog.html.dl



