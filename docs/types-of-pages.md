#Types of Pages

- **List Page:** Lists all the items on the page
	
	A list page can be **paginated or non paginated**.

- **Details Page:** Gives details of one item on the page.

- **Search Page**: To show in-site search results.