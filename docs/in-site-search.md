#In-site search using kitsune

kitsune performs in-site search i.e. it will search within the site (products and updates).

Schema for search is a bit different than our Business Schema.

###Schema:

```
   public class Search
    {
        public string originalsearchtext
        public string modifidedsearchtext
        public IEnumerable<SearchModel> result
        public SearchPagination extra
    }

    public class SearchModel
    {
        public string originalsearchtext
        public string modifidedsearchtext
        public string title
        public string description
        public string tileimage
        public string actualimage
        public DateTime timestamp
        public string itemtype
        public object data
    }
    
   public class SearchPagination
    {
        public int currentindex
        public long totalcount
        public int pagesize
    }
```

```search``` class consists of ```originalsearchtext``` which the user entered in the search field.

kitsune modifies the original search text to ```modifiedsearchtext``` to make it browser friendly.

```results object``` of the ```searchModel``` class gives the results of the search text by the user, which includes the ```title```, ```description```, ```image```, ```itemtype``` etc. of the search result(s).

```itemtype``` property tells us whether the search result returned is of the type products or updates.

```extra object``` of the SearchPagination class gives currentindex, totalcount and pagesize to create paginations or dynamic URL using k-dl tag.

