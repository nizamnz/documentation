# Dashboard

When you log in, you will be taken to the **dashboard** containing all your projects and settings. From the Dashboard, you will be given the option of **optimizing** your existing website by simply **uploading** the website template or migrating an already existing website to kitsune platform.

*If you don't have any idea or material present as of the moment, you can even create a blank template project and start editing it later.*

![dashboard_2](/images/gettingStarted/createNewProject2.png)

On the top right, current wallet balance is displayed and an "[add more funds](doc:payment-info)" button to add more balance to the wallet to continue hosting websites on kitsune. Next, to the wallet balance, you will see your account setting and the logout option in the drop-down.

The dashboard also shows all the existing projects, allows the creation of new projects and live sites and also ana services, billing from the same place and easiness.

![dashboard_1](/images/gettingStarted/uploadDone.png)

