
# My Account & Billing

Click on the profile icon the top right corner of the dashboard to access the **Account settings** tab.

![account_tab_pointer](/images/general/profileChoose.png)

In the My Account tab, you can view your account profile, usage history, and transaction history.

Account Information will consist of your Name, Address, Phone Number and Email information. You can edit the information accordingly and save them.

![account_info](/images/general/profile.png)

Usage history, Storage tab helps you keep track of the number of web requests received every day for your published website.

![usage_history](/images/general/billing1.png)

Transaction history will show the current account balance, non billed amount and the information about all your previous transactions.

![transaction_history1](/images/general/billing2.png)