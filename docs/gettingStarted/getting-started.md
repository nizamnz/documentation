#Host your first website on Kitsune

Not sure how to get started with Kitsune? In this tutorial, you will get a step by step guide on how to upload your static website on Kitsune, how to optimize all your files, assets and finally how to go live!

Please don't hesitate and move forward from the next choice below or choose from the options on your left-hand side. 

Please don't forget to signup for all the action at
[Kitsune](http://www.getkitsune.com)

![Kitsune Web](/images/gettingStarted/kitsuneweb.png)