# How to publish the website?

**Step 1**. Click on the Publish button present on the project menu to publish the website on Kitsune. 

![publish1](/images/gettingStarted/optimizeChoose.png)

**Step 2**. To publish it is necessary to add a minimum amount of Rs. 300 to the wallet. To add money, just click on the "[add more funds](doc:payment-info)" button on the top right of the dashboard.

**Step 3**. Click on publish button to choose from the below 3 options of how you want to publish.

![1](/images/gettingStarted/publishChoiceType.png)

**Step 4**. On next step enter the customer details in the form that appears.

![3](/images/gettingStarted/customerDetails.png)
  
**Step 5**. After filling up the customer details, click on the "proceed" button. Click on 'confirm' button in the next dialog box that appears.

![4](/images/gettingStarted/confirmPublish.png)

In the live sites tab on the dashboard left-hand side, all the published sites are listed, from there just click on the website name and view your **live website** hosted on Kitsune! Click on the menu and **open site**

![dnsPublished](/images/gettingStarted/dnsPublish.png)
![openSite](/images/gettingStarted/dnsOpen.png)

By default, any website hosted on Kitsune is accessible by getkitsune domain, which has the form `[name of website].getkitsune.com`. For example, you would access a website named ‘bluemasters’ as `bluemasters.getkitsune.com`


![dnsDetails](/images/gettingStarted/dnsDetails.png)

!!! note
    You can also [map your own domain to Kitsune](/publish/setting-custom-subdomains.md)!