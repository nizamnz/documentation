#Paginated List Page

**The two important buttons for pagination:**
![](/images/paginations.jpg)

**Code example:**

```
   <div k-show="[[coffeeshop.blogpost.length() > 0]]">
		<ul>
		    <li><a href="[[View('/blog.html.dl').previouspage.url]]">Previous</a></li>  
			<li><a href="[[View('/blog.html.dl').nextpage.url]]">Next</a></li>
		</ul>
	</div>
```

The ```previous``` and ```nextpage``` properties will get the next and previous page URL for the page given in the ```View()``` function i.e. blog.html.dl

####To limit the number of items to be displayed on one page:

To let kitsune know from which index to load the items from, when next/previous button is clicked, use the ```offset``` property with the k-repeat tag instead of given the starting index as 0.


Let's see how:

!!! info
	```offset``` is used with the k-repeat tag for content separation.

**Code example:**

```
<div k-show="[[coffeeshop.blogpost.length() > 0]]">
    <div k-repeat="[[coffeeshop.blogpost,i,View('/blog.html.dl').offset:6]]">
		<div> [[coffeeshop.blogpost[i].description.text.substr(0,150)]]  <div>
	</div>
	<ul>
	    <li><a href="[[View('/blog.html.dl').previouspage.url]]">Previous</a></li>  
		<li><a href="[[View('/blog.html.dl').nextpage.url]]">Next</a></li>
	</ul>
</div>
```

```offset``` start from 0 by default, but when next or previous button is clicked it automatically adjusts its value.

In this example when the page is loaded for the first time, ```offset``` value starts from 0 till 5 and when next button is clicked ```offset``` value starts from 6 and goes till 11.