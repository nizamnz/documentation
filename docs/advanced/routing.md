#Routing
Routing handles the matching of incoming url to the respective file. We use tree traversal algorithms to generate a routing tree for each project from their UrlPatterns (which are generated from respective [k-dl](../k-dl-tag) tags)

Note: segment is the part of url path between two consecutive forward slashes '/'.  
e.g. https://example.com/__this-is-a-segment__/__this-is-another-segment__/

## Precedence Order
Following is the precedence order we follow for sorting all the url patterns in a project.

* Simple urls first (purely static url patterns)
* More complex urls later (complexity is the number of regexes in the url patterns)
    * Regexes closer to the start of segment match first
    * Regexes close to the end of the segment match next
    * Segments completely made up of regex match at the last 

## Optional Segments
To match end user behaviour and cleaner urls, we make segment having only [currentPageNumber](../dynamic-page-properties) as the last segment of the url pattern, optional. This is only for [LIST](../types-of-pages) and [SEARCH](../types-of-pages) page types.

e.g. If you have a k-dl like ```/products/[[View('products.html.dl')currentPageNumber]]```  
Then the page would also be accessible at ```/products/```