# Optimize your Website

Optimize your website by selecting the optimize option available in the drop-down of your project.

![optimize](images/optimize.png)        
		
A pop-up will appear saying "Build Started", which will optimize all your files

![build_started](images/build_started.png)

Kitsune will minify all your HTML, CSS and JS i.e. it will remove all the unnecessary characters like line breaks, white spaces etc. It will also apply GZIP compression on your files and provides CDN (Content Delivery Network) services so that your website loads faster from anywhere in the world.

Oh! Kitsune will also handle versioning to help with file invalidation in case of browser caching.